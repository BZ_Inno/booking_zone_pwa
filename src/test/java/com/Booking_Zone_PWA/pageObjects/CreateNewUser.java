package com.Booking_Zone_PWA.pageObjects;


import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateNewUser
{


	public CreateNewUser(WebDriver driver) 
	{
		PageFactory.initElements(driver, this);

	}

	@FindBy (xpath="(//div//button[text()='Sign In'])[1]")
	WebElement btnSignIn;
	@FindBy (xpath="//input[@id='formBasicEmail']")
	WebElement txtEmail;
	@FindBy (xpath="(//input[@id='formBasicPassword'])[2]")
	WebElement txtPass;
	@FindBy (xpath="//button[text()='Sign Up']")
	WebElement btnSignUp;
	@FindBy (xpath="(//input[@name='firstName'])[2]")
	WebElement txtFirstName;
	@FindBy (xpath="//input[@name='lastName']") 
	WebElement txtLastName;
	@FindBy (xpath="(//input[@name='phone'])[2]")
	WebElement txtPhone;
	@FindBy (xpath="//input[@name='email']")
	WebElement txtEmail1;
	@FindBy (xpath="//input[@name='password']")
	WebElement txtPass1;
	@FindBy (xpath="//input[@id='formVerificationCode']")
	WebElement txtCode;
	@FindBy (xpath="//button[text()='Confirm']")
	WebElement btnConfirm;
	@FindBy (xpath="//button[text()='Login']")
	WebElement btnLogin;
	
	@FindBy (xpath="/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/nav[1]/section[1]/div[2]/span[1]")
	WebElement backbtnBookingHistory;

	@FindBy (xpath="(//button[contains(@class,'Navbar_navbar__toggler___xBzd navbar-toggler collapsed')]")
	WebElement click3dots;

	@FindBy (xpath="//button[text()='Logout']")
	WebElement btnLogout;
	
	
	public void ClickSignIn () 
	{
		btnSignIn.click();
	}

	public void SetEmail(String email) 
	{
		txtEmail.sendKeys(email);
    }	

    public void SetPass(String pass) 
    {
    	txtPass.sendKeys(pass);
    }

     public void ClickSignUp() 
     {
    	 btnSignUp.click();
     }

    public void SetFirstName(String FirstName) 
    {
    	txtFirstName.sendKeys(FirstName);
    }

    public void SetLastName(String LastName) 
    {
    	txtLastName.sendKeys(LastName);
    }

    public void SetPhone(String phone) 
    {
    	txtPhone.sendKeys(phone);
    }

    public void SetEmail2(String Email) 
    {
    	txtEmail1.sendKeys(Email);
    }
    
    public void SetPass2(String pass) 
    {
    	txtPass1.sendKeys(pass);
    }
    public void SetCode(String Code) 
    {
    	txtCode.sendKeys(Code);
    }
    
    public void ClickConfirm() 
    {
    	btnConfirm.click();
    }
    
    public void ClickLogin() 
    {
    	btnLogin.click();
    }
    
    public void clickbackbtn(WebDriver driver ) 
    {
    	WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(30));
		   wait.until(ExpectedConditions.elementToBeClickable(backbtnBookingHistory));  
		   
		   JavascriptExecutor js = (JavascriptExecutor)driver;
		   	js.executeScript("arguments[0].click()", backbtnBookingHistory);
		 
    
    }
    
    public void Click3dots() 
    {
    	click3dots.click();
    	
    }
    
    public void btnLogout() 
    {
    	btnLogout.click();
    }
    
}
