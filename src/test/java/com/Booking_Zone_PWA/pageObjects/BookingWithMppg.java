package com.Booking_Zone_PWA.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BookingWithMppg
{
	

	public BookingWithMppg(WebDriver driver) 
	{
		PageFactory.initElements(driver, this);

	}

	@FindBy (xpath="//button[contains(text(),'Pay')]")
	WebElement btnPay;
	
	@FindBy (xpath="//input[@name='te-connect-secure-pan']")
	WebElement txtcardNo;
	
	@FindBy (xpath="//input[@name='te-connect-secure-expiration']")
	WebElement txtExpiry;
	
	@FindBy (xpath="//input[@name='te-connect-secure-cvc']")
	WebElement txtCVC;
	
	
	public void ClickPaybtn () 
	{
		btnPay.click();
	}
	
	public void setCardNo(String card) 
	{
		txtcardNo.sendKeys(card);
		
	}
	
	public void setExpiry(String expiry) 
	{
		txtExpiry.sendKeys(expiry);
		
	}
	
	public void setCVC(String CVC) 
	{
		txtCVC.sendKeys(CVC);
		
	}

}
