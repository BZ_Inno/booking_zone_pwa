package com.Booking_Zone_PWA.pageObjects;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OrderSumarryPage
{

	
	public OrderSumarryPage(WebDriver driver) 
	{
		
		PageFactory.initElements(driver, this);
		
	}
	
	
	@FindBy (name="promocode") 
	WebElement SetPromo;
	
	@FindBy (xpath="(//button[text()='Apply'])[1]") 
	WebElement btnapply;
	
	@FindBy (xpath="(//div//input[@class='form-control'])[3]") 
	WebElement txtName;
	
	@FindBy (xpath="(//div//input[@class='phone_no form-control'])[2]") 
	WebElement txtContact;
	
	@FindBy (xpath="(//div//input[@class='form-control'])[4]") 
	WebElement txtEmail;
	
	@FindBy (xpath="//button[normalize-space()='Make Booking']") 
	WebElement btnmakebooking;
	
	
	public void SetPromo(String promo) 
	{
		SetPromo.sendKeys(promo);;
	}
	
	public void Clickapply() 
	{
		btnapply.click();
	}
	
	
	public void SetFirstName(String name, WebDriver driver) 
	{
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(50));
		   wait.until(ExpectedConditions.elementToBeClickable(txtName));  
		   
		   JavascriptExecutor js = (JavascriptExecutor)driver;
		   	js.executeScript("arguments[0].click()", txtName);
		 
		txtName.sendKeys(name);
	}
	
	public void SetContact(String contact, WebDriver driver) 
	{
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(50));
		   wait.until(ExpectedConditions.elementToBeClickable(txtContact));  
		   
		   JavascriptExecutor js = (JavascriptExecutor)driver;
		   	js.executeScript("arguments[0].click()", txtContact); 
	
		txtContact.sendKeys(contact);
	}
	public void SetEmail(String Email, WebDriver driver) 
	{
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(50));
		   wait.until(ExpectedConditions.elementToBeClickable(txtEmail));  
		   
		   JavascriptExecutor js = (JavascriptExecutor)driver;
		   	js.executeScript("arguments[0].click()", txtEmail);
		 
		txtEmail.sendKeys(Email);
	}
	
	public void ClickMakeBooking() 
	{
		btnmakebooking.click();
	}
	

	
	
	

}
