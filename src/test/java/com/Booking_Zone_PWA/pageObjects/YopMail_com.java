package com.Booking_Zone_PWA.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YopMail_com
{
    public YopMail_com(WebDriver driver)
    {
    	PageFactory.initElements(driver, this);
    	
    }
	
    @FindBy (xpath="//input[@id='login']") 
    WebElement txtHostName;
    
    @FindBy (xpath="//button[@class='md']") 
    WebElement Arrow;

    
	@FindBy (xpath="//div[contains(text(),'Your verification code is')]")
	WebElement getCode;
	
	public void SetLoginName(String UN) 
	{
		txtHostName.sendKeys(UN);
		
	}
    
	public void ClickArrow() 
	{
		Arrow.click();
	}
    public String getCode()
    {
    	
    String	code=getCode.getText();   //Your verification code is 480097.
    System.out.println(code);
 String  verificationcode=code.substring(26 , 32);
    	System.out.println(verificationcode);
    	
    	return verificationcode;
    }
    
   

	
}
