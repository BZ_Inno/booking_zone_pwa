package com.Booking_Zone_PWA.pageObjects;


import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage
{

	
	public HomePage(WebDriver driver) 
	{
		
		PageFactory.initElements(driver, this);
		
	}
	

	@FindBy (xpath="//button[normalize-space()='Select']") 
	WebElement btnselect;
	
	@FindBy (xpath="(//div//input[@class='form-control'])[3]") 
	WebElement setcityName;
	
	@FindBy (xpath="//div[@role='rowgroup']//div[1]") 
	WebElement selectcity;
	
	@FindBy (xpath="//div[@class='icon_border c_pointer undefined SlickSlider_category__jh-95']") 
	WebElement btnEntertainment;
	
	@FindBy (xpath="(//div[@class='d-flex align-items-center'])[4]") 
	WebElement ScrollToOutlet;

	@FindBy (xpath="//img[@title='Outlet_Demo']") 
	WebElement clickOutlet;

	
	public void ClickSelect(WebDriver driver) 
	{
		 WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(10));
		   wait.until(ExpectedConditions.elementToBeClickable(btnselect));
		   
		 
		       JavascriptExecutor js = (JavascriptExecutor)driver;
		   	js.executeScript("arguments[0].click()", btnselect);
		
		
	}
	
	public void SetCityName(String city, WebDriver driver) 
	{
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(20));
		   wait.until(ExpectedConditions.visibilityOfAllElements(setcityName));
		   
		setcityName.sendKeys(city);
		
	}
	
	public void selectcity() 
	{
		selectcity.click();
		
	}
	
	public void ClickEntertainment() 
	{
		btnEntertainment.click();
		
	}
	public void ScrollToOutlet(WebDriver driver) throws InterruptedException 
	{
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView()", ScrollToOutlet);
		Thread.sleep(2000);
		
	}
	public void clickOutlet() 
	{
		clickOutlet.click();
		
	}

}
