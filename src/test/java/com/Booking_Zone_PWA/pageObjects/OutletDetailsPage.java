package com.Booking_Zone_PWA.pageObjects;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OutletDetailsPage
{

	
	public OutletDetailsPage(WebDriver driver) 
	{
		
		PageFactory.initElements(driver, this);
		
	}
	

	@FindBy (xpath="/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/section[1]/div[1]/div[3]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/img[1]") 
	WebElement SelectPlan;

	@FindBy (xpath="//button//span[contains(@class,'i-bz-')]") 
	WebElement btnCalander;
	
	@FindBy (xpath="//button[text()='Next Month']") 
	WebElement NextMonthArrow;
	
	@FindBy (xpath="//div[text()='7']") 
	WebElement SelectDate;

	@FindBy (xpath="//p[normalize-space()='04:00 AM']") 
	WebElement SelectTime;

	@FindBy (xpath="(//div//button[@class='slot_btn btn btn-outline-primary '])[2]") 
	WebElement SelectDuration;
	
	@FindBy (xpath="(//div//button[@class= 'Counter_btn_input__1lOIS d-flex align-items-center btn btn-gray btn-sm'])[2]") 
	WebElement Selectslot;

	@FindBy (xpath="(//div//button[@class= 'Counter_btn_input__1lOIS d-flex align-items-center btn btn-gray btn-sm'])[4]") 
	WebElement SelectItem;
	
	@FindBy (xpath="//button[normalize-space()='Book Now']") 
	WebElement btnbooknow;
	
	
	public void SelectPlan(WebDriver driver) 
	{
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(30));
		   wait.until(ExpectedConditions.elementToBeClickable(SelectPlan));  
		   
		   JavascriptExecutor js = (JavascriptExecutor)driver;
		   	js.executeScript("arguments[0].click()", SelectPlan);
		 
		
		
	}
	
	public void ClickCalander(WebDriver driver) 
	{
		JavascriptExecutor js = (JavascriptExecutor)driver;
     	js.executeScript("arguments[0].click()", btnCalander);  
		
	}
	
	public void ClickNextMonth(WebDriver driver) 
	{
		JavascriptExecutor js = (JavascriptExecutor)driver;
     	js.executeScript("arguments[0].click()", NextMonthArrow);  
	
	}
	
	public void SelectDate(WebDriver driver) 
	{
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
     	js.executeScript("arguments[0].click()", SelectDate);  
		
		
	}
	
	public void SelectTime(WebDriver driver ) 
	{
		JavascriptExecutor js = (JavascriptExecutor)driver;
     	js.executeScript("arguments[0].click()", SelectTime);  
		
		
	}
	
	public void SelectDuration(WebDriver driver) 
	{
		
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(10));
		   wait.until(ExpectedConditions.elementToBeClickable(SelectDuration));
		   
		 
		       JavascriptExecutor js = (JavascriptExecutor)driver;
		   	js.executeScript("arguments[0].click()", SelectDuration);
		
	}
	
	public void Selectslot(WebDriver driver) 
	{ 
		JavascriptExecutor js = (JavascriptExecutor)driver;
     	js.executeScript("arguments[0].click()", Selectslot);   
	 	  
	}
	
	public void SelectItem(WebDriver driver) 
	{
		JavascriptExecutor js = (JavascriptExecutor)driver;
     	js.executeScript("arguments[0].click()", SelectItem);  
		
		
	}
	
	public void ClickBookNow() 
	{
		btnbooknow.click();
	}

	

}
