package com.Booking_Zone_PWA.testCases;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Booking_Zone_PWA.pageObjects.BookingWithMppg;
import com.Booking_Zone_PWA.pageObjects.CreateNewUser;
import com.Booking_Zone_PWA.pageObjects.HomePage;
import com.Booking_Zone_PWA.pageObjects.OrderSumarryPage;
import com.Booking_Zone_PWA.pageObjects.OutletDetailsPage;
import com.Booking_Zone_PWA.utilities.Xutilities;

public class TC_loginUser_mppg_Booking003 extends BaseClass
{

	HomePage hp;
	OrderSumarryPage sp;
	OutletDetailsPage dp;
	CreateNewUser CU;
	BookingWithMppg mppg;

	Xutilities x;
	ArrayList<String>  ar;


	@BeforeClass
	public void Open_Browser(ITestContext Context) throws IOException 
	{
		SetUp(Context);
		extentTest.info("Browser opened...!");
		log.info("Browser opened...!");
		System.out.println("Browser opened...!");
		
		hp=new HomePage(driver);
		sp=new OrderSumarryPage(driver);
		dp=new OutletDetailsPage(driver);
		CU=new CreateNewUser(driver);
	    mppg=new BookingWithMppg(driver);
	    x=new Xutilities();
	  
	   
	}

	@BeforeMethod
	public void Login() throws IOException, InterruptedException 
	{
		 
		  ar= x.getData("Sheet1","Login Credentials");

		CU.ClickSignIn();
		Thread.sleep(2000);

		CU.SetEmail(ar.get(1));
		Thread.sleep(2000);
		CU.SetPass(ar.get(2));
		Thread.sleep(5000);
		CU.ClickLogin();
		Thread.sleep(10000);
		extentTest.info("User login successfully...!");
		log.info("User login successfully...!");
		System.out.println("User login successfully...!");
		Thread.sleep(5000);
		CU.clickbackbtn(driver);
		Thread.sleep(5000);



	}

	//The Login User should able to create booking with mppg
	@Test (testName="The Login User should able to create booking with mppg")
	public void Createbooking_mppg() throws InterruptedException, IOException
	{

		 ar= x.getData("Sheet1","Create booking with mppg");

		/*hp.ClickSelect();
		 Thread.sleep(3);
		 hp.SetCityName("pe");
		 Thread.sleep(2);
		 hp.selectcity();
		 Thread.sleep(2);*/

		try {

			hp.ClickEntertainment();
			extentTest.info("Clicked on entertainment btn...!");
			log.info("Clicked on entertainment btn...!");
			Thread.sleep(10);
			hp.ClickSelect(driver);
			Thread.sleep(15);
			hp.SetCityName( "pe" , driver);
			extentTest.info("City selected...!");
			log.info("City selected...!");
			Thread.sleep(5);
			hp.selectcity();
			Thread.sleep(2);
			hp.ScrollToOutlet(driver);
			Thread.sleep(5);
			hp.clickOutlet();
			Thread.sleep(5);
			dp.SelectPlan(driver);
			extentTest.info("Plan selected for booking...!");
			log.info("Plan selected for booking...!");
			Thread.sleep(10);
			dp.ClickCalander(driver);
			Thread.sleep(5);
			dp.ClickNextMonth(driver);
			Thread.sleep(5);
			dp.SelectDate(driver);
			Thread.sleep(10);
			dp.SelectTime(driver);
			Thread.sleep(10);
			dp.SelectDuration(driver);
			extentTest.info("Date,duration selected...!");
			log.info("Date,duration selected...!");
			Thread.sleep(10);
			dp.Selectslot(driver);
			Thread.sleep(5);
			dp.SelectItem(driver);
			extentTest.info("Slot, items selected...!");
			log.info("Slot,items selected...!");
			Thread.sleep(5);
			dp.ClickBookNow();
			extentTest.info("Clicked on book now btn...!");
			log.info("Clicked on book now btn...!");
			Thread.sleep(10);
			mppg.ClickPaybtn();
			extentTest.info("Clicked on pay btn...!");
			log.info("Clicked on pay btn...!");
			Thread.sleep(2);
			
			//switch focus to frame
			driver.switchTo().frame("__te-connect-secure-window");
			
			mppg.setCardNo(ar.get(1));
			Thread.sleep(2);
			mppg.setExpiry(ar.get(2));
			Thread.sleep(2);
			mppg.setCVC(ar.get(3));
			Thread.sleep(2);
			extentTest.info("Card details setUp...!");
			log.info("Card details setUp...!");
			
			//switch focus back to main page
			driver.switchTo().defaultContent();
			
			mppg.ClickPaybtn();
			Thread.sleep(10);
			extentTest.info("Booking created successfully with mppg...!");
			log.info("Booking created successfully with mppg...!");
			System.out.println("Booking created successfully with mppg...!");
		}
		catch(Exception e) 
		{
			e.getStackTrace();
			String issueCause = e.getLocalizedMessage();
			System.out.println("issue cause is :" + issueCause);
		}

		String  ActualResult=driver.findElement(By.xpath("//div//p[text()='Booking successful']")).getText();
		String ExpectedResult="Booking successful";
		Assert.assertEquals(ActualResult, ExpectedResult);


	}

	



}





