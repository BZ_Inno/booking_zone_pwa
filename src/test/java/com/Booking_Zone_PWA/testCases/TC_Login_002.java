package com.Booking_Zone_PWA.testCases;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Booking_Zone_PWA.pageObjects.CreateNewUser;
import com.Booking_Zone_PWA.pageObjects.YopMail_com;
import com.Booking_Zone_PWA.utilities.Xutilities;

public class TC_Login_002 extends BaseClass
{
	 CreateNewUser CU;
	 YopMail_com YM;
	 Xutilities	 x;
		ArrayList<String>  ar;
	 
	 @BeforeClass
	 public void open_Browser(ITestContext context) throws IOException 
	 {
		    SetUp(context);
		    extentTest.info("Browser opened...!");
			log.info("Browser opened...!");
			System.out.println("Browser opened...!");
			
		    CU=new CreateNewUser(driver);
			YM=new YopMail_com(driver);
			 x=new Xutilities();
		
				
     }
	 


	@Test (testName="Login")
	public void CreateUser_Account() throws InterruptedException 
	{
	
		try 
		{
			  ar= x.getData("Sheet1","Login Credentials");
			
		CU.ClickSignIn();
		Thread.sleep(2000);
		
		CU.SetEmail(ar.get(1));
		Thread.sleep(2000);
		CU.SetPass(ar.get(2));
		Thread.sleep(5000);
		CU.ClickLogin();
		Thread.sleep(10000);
		
		extentTest.info("User logged in successfully......!");
		log.info("User logged in successfully......!");
        System.out.println("user logged in successfully...");
        
        
		}
		catch (Exception ad)
		{
			ad.getStackTrace();
			String issueCause = ad.getLocalizedMessage();
			System.out.println("issue cause is :" + issueCause);
		}
		
		// apply assertion 
		
		String  ActualResult=driver.findElement(By.xpath("(//div//span[text()='Booking History'])[2]")).getText();
		String ExpectedResult="Booking History";
		Assert.assertEquals(ActualResult, ExpectedResult);

		
	}
	




}
