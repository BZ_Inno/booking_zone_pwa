package com.Booking_Zone_PWA.testCases;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.Booking_Zone_PWA.utilities.Xutilities;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass 
{
	public static  WebDriver driver;
	public static ExtentReports extentReport;
	public static String screenshotsSubFolderName;
	public static ExtentTest extentTest;
	
	public static Logger log= LogManager.getLogger("BaseClass");
	
	
	
	public void SetUp(ITestContext context) 
	{
		 WebDriverManager.chromedriver().setup();
		 ChromeOptions option=new ChromeOptions();
		  option.addArguments("--remote-allow-origins=*");
		  driver=new ChromeDriver(option);
		
		driver.get("https://test.bookingzone.com");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.manage().window().maximize();
		
		
		Capabilities capabilities = ((RemoteWebDriver) driver).getCapabilities();
		String device = capabilities.getBrowserName() + " " + capabilities.getBrowserVersion().substring(0, capabilities.getBrowserVersion().indexOf("."));
		String author = context.getCurrentXmlTest().getParameter("author");

		extentTest = extentReport.createTest(context.getName());
		extentTest.assignAuthor(author);
		extentTest.assignDevice(device);
	}
	
	// initialize the extent reporter 
	@BeforeSuite
	public void Initialize_ExtentReport() 
	{
		ExtentSparkReporter spark=new ExtentSparkReporter(".\\PWA_Report.html");
		spark.config().setReportName("PWA_TestReport");
		spark.config().setTheme(Theme.DARK);
		spark.config().setDocumentTitle("Extent Report");
		
		extentReport=new ExtentReports();
		extentReport.attachReporter(spark);
		extentReport.setSystemInfo("OS", System.getProperty("os.name"));
		extentReport.setSystemInfo("Java Version", System.getProperty("java.version"));
		
	}
	
	@AfterSuite
	public void GenerateReport() 
	{
		extentReport.flush();
	}
	
	@AfterMethod
	public void CheckStatus(Method m, ITestResult result ) 
	{
		if (result.getStatus()== ITestResult.FAILURE) 
		{
			try 
			{
			          String ScreenshotPath=null;
				
				ScreenshotPath=CaptureScreenshot(result.getName() +".jpg");
				extentTest.addScreenCaptureFromPath(ScreenshotPath);
				System.out.println("ScreenshotPath is:-"+ ScreenshotPath);
				extentTest.fail(result.getThrowable());

			}
			catch(Exception e) 
			{
				e.getStackTrace();
			}
			
			
		}
		else if (result.getStatus()==ITestResult.SUCCESS) 
		{
			extentTest.pass(m.getName() + " is passed");
			
		}
			
		
	}
	
	public String CaptureScreenshot(String fileName) throws IOException 
	{
		if (screenshotsSubFolderName==null) 
		{
			 LocalDateTime myDateObj = LocalDateTime.now();
			 DateTimeFormatter myFormaterObj= DateTimeFormatter.ofPattern("DD-MM-YY");
			 screenshotsSubFolderName =myDateObj.format(myFormaterObj);
		}
		
		 TakesScreenshot SS=(TakesScreenshot)driver;
		 File soarcefile=SS.getScreenshotAs(OutputType.FILE);
		
		 File destfile=new File(".\\Screenshots\\" + screenshotsSubFolderName+ fileName );  
		 
		 try {
				FileUtils.copyFile(soarcefile, destfile);
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Screenshot saved successfully: " + destfile.getCanonicalPath().substring(2));
			return destfile.getCanonicalPath().substring(2);

		}
	
	@AfterTest
	public void teardown() 
	{
		
		driver.quit();
		extentTest.info("Browser closed...!");
		log.info("Browser closed...!");
		System.out.println("Browser closed...!");
	}
	

}
