package com.Booking_Zone_PWA.testCases;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.Booking_Zone_PWA.pageObjects.CreateNewUser;
import com.Booking_Zone_PWA.pageObjects.YopMail_com;
import com.Booking_Zone_PWA.utilities.Xutilities;

public class TC_CreateUserAccount_001 extends BaseClass
{
	CreateNewUser CU;
	YopMail_com YM;
	Xutilities  x;


	@BeforeClass
	public void open_Browser(ITestContext context) 
	{
		SetUp(context);
		extentTest.info("Browser opened...!");
		log.info("Browser opened...!");
		System.out.println("Browser opened...!");
		CU=new CreateNewUser(driver);
		YM=new YopMail_com(driver);
		x=new Xutilities();
	}
	
	
//User should able to create account from home page 
	@Test (testName="CreateUser_Account_OnHomePage")
	public void CreateUser_Account() throws InterruptedException 
	{
		

		try 
		{
			ArrayList<String> ar= x.getData("Sheet1","Create user account via home page");

			CU.ClickSignIn();
			extentTest.info("Ckicked on signIN button...!");
			log.info("Ckicked on signIN button...!");
			Thread.sleep(2000);
			CU.ClickSignUp();
			extentTest.info("Ckicked on signUp button...!");
			log.info("Ckicked on signUp button...!");
			Thread.sleep(2000);
			CU.SetFirstName(ar.get(1));
			extentTest.info("First name setup...!");
			log.info("First name setup...!");
			Thread.sleep(2000);
			CU.SetLastName(ar.get(2));
			extentTest.info("Last name setup...!");
			log.info("Last name setup...!");
			Thread.sleep(2000);
			CU.SetPhone(ar.get(3));
			extentTest.info("PhoneNo name setup...!");
			log.info("PhoneNo name setup...!");
			Thread.sleep(2000);
			CU.SetEmail2(ar.get(4));
			extentTest.info("Email setup...!");
			log.info("Email setup...!");
			Thread.sleep(2000);
			CU.SetPass2(ar.get(5));
			extentTest.info("Password setup...!");
			log.info("Password setup...!");
			Thread.sleep(2000);
			CU.ClickSignUp();
			extentTest.info("Clicked on singUp Btn...!");
			log.info("Clicked on singUp Btn...!");
			Thread.sleep(2000);
			System.out.println("Clicked on signup btn...!");


			//keyboard shortcut to open new tab 
			((JavascriptExecutor) driver).executeScript("window.open();");

			// switch focus to the newly open tab 
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));
			extentTest.info("focus Switched to the newly open tab...!");
			log.info("focus Switched to the newly open tab...!");
			System.out.println("focus Switched to the newly open tab...!");

			driver.get("https://yopmail.com");
			Thread.sleep(5000);
			extentTest.info("YopMail opened...!");
			log.info("YopMail opened...!");

			YM.SetLoginName(ar.get(6));
			Thread.sleep(2000);
			YM.ClickArrow();
			extentTest.info("Logged in yopmail...!");
			log.info("Logged in yopmail...!");
			Thread.sleep(5000);

			// switch focus to the frame
			driver.switchTo().frame("ifmail");
			extentTest.info("focus Switched to the frame...!");
			log.info("focus Switched to the frame...!");

			String  CODE=YM.getCode();
			System.out.println(CODE);
			Thread.sleep(2000);

			driver.switchTo().defaultContent();
			driver.close();

			//SWITCH FOCUS TO Main window 
			driver.switchTo().window(tabs.get(0));
			extentTest.info("Yopmail closed and focus shifted back to main window...!");
			log.info("Yopmail closed and focus shifted back to main window...!");

			CU.SetCode(CODE);
			Thread.sleep(2000);
			CU.ClickConfirm();
			Thread.sleep(5000);
			CU.ClickSignIn();
			extentTest.info("Confirmation code entered and signIn...!");
			log.info("Confirmation code entered and signIn...!");
			Thread.sleep(2000);
			CU.SetEmail(ar.get(4));
			Thread.sleep(2000);
			CU.SetPass(ar.get(5));
			Thread.sleep(2000);
			CU.ClickLogin();
			Thread.sleep(2000);
			extentTest.info("Sucessfully Login with newly created account...!");
			log.info("ucessfully Login with newly created account...!");
			System.out.println("Sucessfully Login with newly created account...");
			driver.quit();



		}
		catch (Exception ad)
		{
			ad.getStackTrace();
			String issueCause = ad.getLocalizedMessage();
			System.out.println("issue cause is :" + issueCause);
		}

		// apply assertion 

		String  ActualResult=driver.findElement(By.xpath("//a[normalize-space()='Booking History']")).getText();
		String ExpectedResult="Booking History";
		Assert.assertEquals(ActualResult, ExpectedResult);
		


	}




}
