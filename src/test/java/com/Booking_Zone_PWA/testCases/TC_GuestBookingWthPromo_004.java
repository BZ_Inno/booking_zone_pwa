package com.Booking_Zone_PWA.testCases;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Booking_Zone_PWA.pageObjects.HomePage;
import com.Booking_Zone_PWA.pageObjects.OrderSumarryPage;
import com.Booking_Zone_PWA.pageObjects.OutletDetailsPage;
import com.Booking_Zone_PWA.utilities.Xutilities;

public class TC_GuestBookingWthPromo_004 extends BaseClass
{

	HomePage hp;
	OrderSumarryPage sp;
	OutletDetailsPage dp;

	Xutilities x;

	ArrayList<String> ar;

	@BeforeClass
	public void Open_Browser(ITestContext Context) 
	{
		SetUp(Context);
		extentTest.info("Browser opened...!");
		log.info("Browser opened...!");
		System.out.println("Browser opened...!");
		
		hp=new HomePage(driver);
		sp=new OrderSumarryPage(driver);
		dp=new OutletDetailsPage(driver);
		x=new Xutilities();
	}



	// The Guest user should able to create booking with 100% promo
	@Test(testName="GuestBookingwith_100Promo")
	public void GuestBookingwith_100Promo() throws InterruptedException
	{
		/*hp.ClickSelect();
		 Thread.sleep(3);
		 hp.SetCityName("pe");
		 Thread.sleep(2);
		 hp.selectcity();
		 Thread.sleep(2);*/
		try { 
			
			ar= x.getData("Sheet1","Create booking with mppg");
			hp.ClickEntertainment();
			extentTest.info("Clicked on entertainment btn...!");
			log.info("Clicked on entertainment btn...!");
			Thread.sleep(10);
			hp.ClickSelect(driver);
			Thread.sleep(15);
			hp.SetCityName("pe", driver);
			Thread.sleep(5);
			hp.selectcity();
			extentTest.info("City selected...!");
			log.info("City selected...!");
			Thread.sleep(2);
			hp.ScrollToOutlet(driver);
			Thread.sleep(5);
			hp.clickOutlet();
			Thread.sleep(5);
			dp.SelectPlan(driver);
			extentTest.info("Plan selected for booking...!");
			log.info("Plan selected for booking...!");
			Thread.sleep(10);
			dp.ClickCalander(driver);
			Thread.sleep(2);
			dp.ClickNextMonth(driver);
			Thread.sleep(2);
			dp.SelectDate(driver);
			Thread.sleep(10);
			dp.SelectTime(driver);
			Thread.sleep(10);
			dp.SelectDuration(driver);
			extentTest.info("Date,duration selected...!");
			log.info("Date,duration selected...!");
			Thread.sleep(10);
			dp.Selectslot(driver);
			Thread.sleep(5);
			dp.SelectItem(driver);
			extentTest.info("Slot, items selected...!");
			log.info("Slot,items selected...!");
			Thread.sleep(5);
			dp.ClickBookNow();
			extentTest.info("Clicked on book now btn...!");
			log.info("Clicked on book now btn...!");
			Thread.sleep(10);
			sp.SetPromo("Sale100");
			Thread.sleep(5);
			sp.Clickapply();
			extentTest.info("Promocode set and applied...!");
			log.info("Promocode set and applied...!");
			Thread.sleep(20);
			sp.SetFirstName("Dip", driver);
			Thread.sleep(5);
			sp.SetContact("2334365475", driver);
			Thread.sleep(5);
			sp.SetEmail("dip@yopmail.com", driver);
			extentTest.info("Contact info setUp...!");
			log.info("Contact info setUp...!");
			Thread.sleep(7);
			sp.ClickMakeBooking();
			extentTest.info("Booking created successfully with 100% promo...!");
			log.info("Booking created successfully with 100% promo...!");
			System.out.println("Booking created successfully with 100% promo...!");

		}
		catch(Exception e)
		{
			e.getStackTrace();
			String issueCause=e.getLocalizedMessage();
			System.out.println("issue cause is :" + issueCause);
		}

		String  ActualResult=driver.findElement(By.xpath("//div//p[text()='Booking successful']")).getText();
		String ExpectedResult="Booking Successful";
		Assert.assertEquals(ActualResult, ExpectedResult);


	}

	
}




