package com.Booking_Zone_PWA.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class Xutilities 
{
	
	public ArrayList<String> getData(String sheetName,String TestCaseName) throws IOException 
	{
		ArrayList<String> ar=new ArrayList<String>();
		
		String file="C:\\Users\\dipal\\eclipse-workspace\\Booking_Zone_PWA\\Test_Data"
				+ "\\PWA_TestData.xlsx";
		
		FileInputStream fis=new FileInputStream(file);
	
		
		XSSFWorkbook workbook=new XSSFWorkbook(fis);
		int NoOfSheets=workbook.getNumberOfSheets();
		XSSFSheet sheets=workbook.getSheet("Sheet1");
		int rowCount=sheets.getPhysicalNumberOfRows();
		int columnCount=sheets.getRow(0).getLastCellNum();
		
		   //Iterator<Row>  rowIterator=sheet.rowIterator();
		   //rowIterator.next();
		   
		   
	for (int i=0; i<NoOfSheets; i++)	   
	{	   
		 if (workbook.getSheetName(i).equalsIgnoreCase(sheetName)) 
		 {
			 XSSFSheet  sheet=workbook.getSheetAt(i);
			   Iterator<Row> rows=sheet.rowIterator(); // rowIterator contains all info of rows
			  Row firstRow=rows.next();
			  Iterator<Cell> cells=firstRow.cellIterator();
			   
			  int k=0;
				int column=0;
			  
		   while (cells.hasNext()) 
		   {
			 Cell  value=cells.next();
			  
			 if(value.getStringCellValue().equalsIgnoreCase("Test Cases"))
			 {
				 //column=k;
				 column=value.getColumnIndex();
				 break;
			 }
			 //k++;
			 
		   } 
		   
			  while(rows.hasNext()) 
			  {
				  Row r=rows.next();
				   Cell cell=r.getCell(column);
				 
				if (cell != null && cell.getStringCellValue().equalsIgnoreCase(TestCaseName)) 
				{
					Iterator<Cell> c=r.cellIterator();
					
					while(c.hasNext()) 
					{
						 Cell CellValue=c.next();
						 
						 if (CellValue.getCellType()==CellType.STRING) 
						 {
							 
							 ar.add(CellValue.getStringCellValue());
							 
						 }
						 else
						 {
							 ar.add(NumberToTextConverter.toText(CellValue.getNumericCellValue()));
						 }
						
					}
					
					
				}
				
				  
			  }
			 
			  
		   }
		
	}
	
	return ar;
	
		}
	
	

}


		   
	
	
	

